#bonus
class ComputerPlayer
  attr_accessor :board
  attr_reader :name, :moves_made

  def initialize(name)
    @name = name
    @moves_made = []
  end

  def display(board)
    @board = board
  end

  def last_move_was_a_hit?(spot)
    return false if @moves_made.last.nil?
    board.[](@moves_made.last) == :h &&
      (spot[0] == @moves_made.last[0] || spot[1] == @moves_made.last[1])
  end

  def available_spots
    size = board.grid.size - 1
    spots = []
    (0..size).each do |row|
      (0..size).each do |col|
        spot = [row, col]
        spots << spot unless @moves_made.include?(spot)
      end
    end
    spots
  end

  def get_play
    available_spots.each do |spot|
      if last_move_was_a_hit?(spot)
        @moves_made << spot
        return spot
      end
    end
    play = available_spots.sample
    @moves_made << play
    play
  end

end
