#bonus

class Ship
  attr_reader :name
  attr_accessor :size, :marker

  def initialize(name, size = nil, marker = nil)
    @name = name
    @size = size
    @marker = marker
  end

  def self.aircraft_carrier
    new("aircraft carrier", 5, :ac)
  end

  def self.battleship
    new("battleship", 4, :bs)
  end

  def self.submarine
    new("submarine", 3, :sm)
  end

  def self.destroyer
    new("destroyer", 3, :ds)
  end

  def self.patrol_boat
    new("patrol boat", 2, :pb)
  end

  def coordindate_maker
    row = (0..9).to_a.sample
    col = (0..9).to_a.sample
    [row, col]
  end

  def orientation_placement
    vertical = 'v'
    horizontal = 'h'
    [vertical, horizontal].sample
  end

  def ship_builder(ship)
    spot = coordindate_maker
    orientation = orientation_placement
    if orientation == 'h'
      ship_spots = row_coordinate_maker(ship, spot)
    elsif orientation == 'v'
      ship_spots = vertical(ship, spot)
    end
    all_spots_valid?(ship_spots) ? ship_spots : ship_builder(ship)
  end

  def row_coordinate_maker(ship, spot)
    spots = [spot]
    row, col = spot
    (ship.size - 1).times do
      col += 1
      spots << [row, col]
    end
    spots
  end

  def col_coordinate_maker(ship, spot)
    spots = [spot]
    row, col = spot
    (ship.size - 1).times do
      row += 1
      spots << [row, col]
    end
    spots
  end

  def all_spots_valid?(ship_spots)
    ship_spots.all? { |spot| spot.nil? && valid_move?(spot) }
  end
end
