class Board
  attr_reader :grid

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.count(:s)
  end

  def won?
    count.zero?
  end

  def full?
    @grid.flatten.none?(&:nil?)
  end

  def empty?(spot = nil)
    if spot.nil?
      count.zero?
    elsif self.[](spot).nil?
      true
    end
  end

  def place_random_ship
    self.[]=(available_spots.sample, :s)
  end

  def available_spots
    available_spots = []
    size = @grid.size - 1
    (0..size).each do |row|
      (0..size).each do |col|
        spot = [row, col]
        available_spots << spot if empty?(spot)
      end
    end
    available_spots
  end

  def in_range?(spot)
    spot.all? { |num| num >= 0 && num <= 9 }
  end

  def [](spot)
    row, col = spot
    grid[row][col]
  end

  def []=(spot, value)
    row, col = spot
    grid[row][col] = value
  end

  def header
    col_nums = (0..9).to_a.join("    |    ")
    title =  half_line + "Battleship The Game" + half_line
    cols = '    |    ' + col_nums + '    |'
    [title, cols, line_break]
  end

  def hide_ships
    result = grid.map do |row|
      row.map do |col|
        col == :s || col.nil? ? "         |" : "    #{col}    |"
      end
    end
    result.map.with_index { |row, idx| row.unshift(" #{idx}  |") }
  end

  def line_break
    line = ""
    105.times { line += "-" }
    line
  end

  def half_line
    line = ""
    43.times { line += "-" }
    line
  end

  def display
    header.each {|line| puts line }
    result = hide_ships
    result.each do |row|
      puts row.join("")
      puts line_break
    end
  end

end
