class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "#{name}, where would you like to strike? Ex: '0,0'"
    move = gets.split(",").map(&:to_i)
    move
  end

end
