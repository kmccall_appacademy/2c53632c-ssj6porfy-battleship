require_relative "board"
require_relative "player"


class BattleshipGame
  attr_accessor :board, :player1

  def initialize(player1, board)
    @player1 = player1
    @board = board
  end

  def attack(spot)
    display_status
    if board.empty?(spot)
      board.[]=(spot, :x)
      puts "You Missed!"
    elsif board.[](spot) == :s
      board.[]=(spot, :h)
      puts "You Struck a battleship!"
    end
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def display_status
    board.display
    puts "There are #{count} enemy ships remaining!"
  end

  def play_turn
    move = player1.get_play
    if board.in_range?(move)
      attack(move)
    else
      puts "Invalid Entry, Please Try Again!"
      play_turn
    end
  end

  def play
    board.place_random_ship until count == 17
    play_turn until game_over?
    puts "Congratulations #{player1.name}!  You sunk all the Battleships!!"
  end

end

# if __FILE__ == $PROGRAM_NAME
#   p1 = HumanPlayer.new("porfy")
#   b1 = Board.new
#   g = BattleshipGame.new(p1, b1)
#   g.play
# end
